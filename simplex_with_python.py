"""
Read-me:
Call functions in this order:
    problem = gen_matrix(v,c)
    constrain(problem, string)
    obj(problem, string)
    maxz(problem)
gen_matrix() produces a matrix to be given constraints and an objective function to maximize or minimize.
    It takes var (variable number) and cons (constraint number) as parameters.
    gen_matrix(2,3) will create a 4x7 matrix by design.
constrain() constrains the problem. It takes the problem as the first argument and a string as the second. The string should be
    entered in the form of 1,2,G,10 meaning 1(x1) + 2(x2) >= 10.
    Use 'L' for <= instead of 'G' and 'E' for =
Use obj() only after entering all constraints, in the form of 1,2,0 meaning 1(x1) +2(x2) +0
    The final term is always reserved for a constant and 0 cannot be omitted.
Use maxz() to solve a maximization LP problem. Use minz() to solve a minimization problem.
Disclosure -- pivot() function, subcomponent of maxz() and minz(), has a couple bugs. So far, these have only occurred when
    minz() has been called.


Lisez-moi:
Appelez les fonctions dans cet ordre:
    problem = gen_matrix(v,c)
    constrain(problem, string)
    obj(problem, string)
    maxz(problem)
gen_matrix() produit une matrice à laquelle sont données des contraintes et une fonction objective à maximiser ou minimiser.
    Il prend var (nombre de variable) et cons (nombre de contrainte) comme paramètres.
    gen_matrix(2,3) créera une matrice 4x7 par conception.
constrain() limite le problème. Il prend le problème comme premier argument et une chaîne comme second. La chaîne doit être
    saisi sous la forme 1,2, G, 10 signifiant 1 (x1) + 2 (x2)> = 10.
    Utilisez 'L' pour <= au lieu de 'G' et 'E' pour =
Utilisez obj() seulement après avoir entré toutes les contraintes, sous la forme de 1,2,0 signifiant 1 (x1) +2 (x2) +0
    Le terme final est toujours réservé à une constante et 0 ne peut pas être omis.

Utilisez maxz() pour résoudre un problème de maximisation LP. Utilisez minz() pour résoudre un problème de minimisation.
Divulgation - la fonction pivot(), sous-composant de maxz() et minz(), a quelques bugs. Jusqu'à présent, ils ne se sont produits que lorsque
    minz() a été appelé.
"""

import numpy as np

# generates an empty matrix with adequate size for variables and constraints.
# génère une matrice vide avec une taille adéquate pour les variables et les contraintes.


def gen_matrix(var, cons):
    tab = np.zeros((cons+1, var+cons+2))
    return tab

# checks the furthest right column for negative values ABOVE the last row. If negative values exist, another pivot is required.
# vérifie la colonne la plus à droite pour les valeurs négatives AU-DESSUS de la dernière ligne.
# Si des valeurs négatives existent, un autre pivot est requis.


def next_round_r(table):
    m = min(table[:-1, -1])
    if m >= 0:
        return False
    else:
        return True

# checks that the bottom row, excluding the final column, for negative values. If negative values exist, another pivot is required.
# vérifie que la ligne du bas, à l'exclusion de la dernière colonne, contient des valeurs négatives.
# Si des valeurs négatives existent, un autre pivot est requis.


def next_round(table):
    lr = len(table[:, 0])
    m = min(table[lr-1, :-1])
    if m >= 0:
        return False
    else:
        return True

# Similar to next_round_r function, but returns row index of negative element in furthest right column
# Similaire à la fonction next_round_r, mais renvoie l'index de ligne de l'élément négatif dans la colonne la plus à droite


def find_neg_r(table):
    # lc = number of columns, lr = number of rows
    lc = len(table[0, :])
    # search every row (excluding last row) in final column for min value
    # recherche chaque ligne (à l'exclusion de la dernière ligne) de la dernière colonne pour la valeur minimale
    m = min(table[:-1, lc-1])
    if m <= 0:
        # n = row index of m location
        n = np.where(table[:-1, lc-1] == m)[0][0]
    else:
        n = None
    return n

# returns column index of negative element in bottom row
# renvoie l'index de la colonne de l'élément négatif dans la ligne du bas


def find_neg(table):
    lr = len(table[:, 0])
    m = min(table[lr-1, :-1])
    if m <= 0:
        # n = row index for m
        n = np.where(table[lr-1, :-1] == m)[0][0]
    else:
        n = None
    return n

# locates pivot element in tableu to remove the negative element from the furthest right column.
# localise l'élément pivot dans le tableau pour supprimer l'élément négatif de la colonne la plus à droite.


def loc_piv_r(table):
    total = []
    # r = row index of negative entry
    # r = index de ligne de l'entrée négative
    r = find_neg_r(table)
    # finds all elements in row, r, excluding final column
    # trouve tous les éléments de la ligne, r, à l'exclusion de la dernière colonne
    row = table[r, :-1]
    # finds minimum value in row (excluding the last column)
    # trouve la valeur minimale dans la ligne (à l'exclusion de la dernière colonne)
    m = min(row)
    # c = column index for minimum entry in row
    # c = index de colonne pour l'entrée minimale dans la ligne
    c = np.where(row == m)[0][0]
    # all elements in column
    # tous les éléments de la colonne
    col = table[:-1, c]
    # need to go through this column to find smallest positive ratio
    # besoin de parcourir cette colonne pour trouver le plus petit ratio positif
    for i, b in zip(col, table[:-1, -1]):
        # i cannot equal 0 and b/i must be positive.
        # i ne peut pas être égal à 0 et b / i doit être positif.
        if i**2 > 0 and b/i > 0:
            total.append(b/i)
        else:
            # placeholder for elements that did not satisfy the above requirements. Otherwise, our index number would be faulty.
            # espace réservé pour les éléments qui ne satisfont pas aux exigences ci-dessus. Sinon, notre numéro d'index serait défectueux.
            total.append(0)
    element = max(total)
    for t in total:
        if t > 0 and t < element:
            element = t
        else:
            continue

    index = total.index(element)
    return [index, c]
# similar process, returns a specific array element to be pivoted on.
# processus similaire, retourne un élément de tableau spécifique sur lequel pivoter.


def loc_piv(table):
    if next_round(table):
        total = []
        n = find_neg(table)
        for i, b in zip(table[:-1, n], table[:-1, -1]):
            if i**2 > 0 and b/i > 0:
                total.append(b/i)
            else:
                # placeholder for elements that did not satisfy the above requirements. Otherwise, our index number would be faulty.
                # espace réservé pour les éléments qui ne satisfont pas aux exigences ci-dessus. Sinon, notre numéro d'index serait défectueux.
                total.append(0)
        element = max(total)
        for t in total:
            if t > 0 and t < element:
                element = t
            else:
                continue

        index = total.index(element)
        return [index, n]

# Takes string input and returns a list of numbers to be arranged in table
# Prend une entrée de chaîne et renvoie une liste de nombres à organiser dans un tableau


def convert(eq):
    eq = eq.split(',')
    if 'G' in eq:
        g = eq.index('G')
        del eq[g]
        eq = [float(i)*-1 for i in eq]
        return eq
    if 'L' in eq:
        l = eq.index('L')
        del eq[l]
        eq = [float(i) for i in eq]
        return eq
    if 'E' in eq:
        e = eq.index('E')
        del eq[e]
        eq = [float(i) for i in eq]
        return eq

# The final row of the tablue in a minimum problem is the opposite of a maximization problem so elements are multiplied by (-1)
# La dernière ligne du tableau dans un problème minimum est l'opposé d'un problème de maximisation donc les éléments sont multipliés par (-1)


def convert_min(table):
    table[-1, :-2] = [-1*i for i in table[-1, :-2]]
    table[-1, -1] = -1*table[-1, -1]
    return table

# generates x1,x2,...xn for the varying number of variables.
# génère x1, x2, ... xn pour le nombre variable de variables.


def gen_var(table):
    lc = len(table[0, :])
    lr = len(table[:, 0])
    var = lc - lr - 1
    v = []
    for i in range(var):
        v.append('x'+str(i+1))
    return v

# pivots the tableau such that negative elements are purged from the last row and last column
# fait pivoter le tableau de manière à ce que les éléments négatifs soient purgés de la dernière ligne et de la dernière colonne


def pivot(row, col, table):
    # number of rows
    lr = len(table[:, 0])
    # number of columns
    lc = len(table[0, :])
    t = np.zeros((lr, lc))
    pr = table[row, :]
    if table[row, col]**2 > 0:  # new
        e = 1/table[row, col]
        r = pr*e
        for i in range(len(table[:, col])):
            k = table[i, :]
            c = table[i, col]
            if list(k) == list(pr):
                continue
            else:
                t[i, :] = list(k-r*c)
        t[row, :] = list(r)
        return t
    else:
        print('Cannot pivot on this element.')

# checks if there is room in the matrix to add another constraint
# vérifie s'il y a de la place dans la matrice pour ajouter une autre contrainte


def add_cons(table):
    lr = len(table[:, 0])
    # want to know IF at least 2 rows of all zero elements exist
    # veux savoir SI au moins 2 lignes de tous les éléments nuls existent
    empty = []
    # iterate through each row
    # parcourir chaque ligne
    for i in range(lr):
        total = 0
        for j in table[i, :]:
            # use squared value so (-x) and (+x) don't cancel each other out
            # utiliser une valeur au carré pour que (-x) et (+ x) ne s'annulent pas
            total += j**2
        if total == 0:
            # append zero to list ONLY if all elements in a row are zero
            # ajouter zéro à la liste UNIQUEMENT si tous les éléments d'une ligne sont à zéro
            empty.append(total)
    # There are at least 2 rows with all zero elements if the following is true
    # Il y a au moins 2 lignes avec tous les éléments nuls si ce qui suit est vrai
    if len(empty) > 1:
        return True
    else:
        return False

# adds a constraint to the matrix
# ajoute une contrainte à la matrice


def constrain(table, eq):
    if add_cons(table) == True:
        lc = len(table[0, :])
        lr = len(table[:, 0])
        var = lc - lr - 1
        # configurer le compteur pour parcourir la longueur totale des lignes
        j = 0
        while j < lr:
            # Itérer par ligne
            row_check = table[j, :]
            # total sera la somme des entrées dans la ligne
            total = 0
            # Trouver la première ligne avec toutes les 0 entrées
            for i in row_check:
                total += float(i**2)
            if total == 0:
                # Nous avons trouvé la première ligne avec toutes les entrées nulles
                row = row_check
                break
            j += 1

        eq = convert(eq)
        i = 0
        # parcourir tous les termes de la fonction de contrainte, à l'exclusion du dernier
        while i < len(eq)-1:
            # attribuer des valeurs de ligne en fonction de l'équation
            row[i] = eq[i]
            i += 1
        #row[len(eq)-1] = 1
        row[-1] = eq[-1]
        # ajouter une variable d'écart en fonction de l'emplacement dans le tableau.
        row[var+j] = 1
    else:
        print('Cannot add another constraint.')

# checks to determine if an objective function can be added to the matrix
# vérifie si une fonction objectif peut être ajoutée à la matrice


def add_obj(table):
    lr = len(table[:, 0])
    # want to know IF exactly one row of all zero elements exist
    # veux savoir SI exactement une ligne de tous les éléments nuls existe
    empty = []
    # iterate through each row
    # parcourir chaque ligne
    for i in range(lr):
        total = 0
        for j in table[i, :]:
            # use squared value so (-x) and (+x) don't cancel each other out
            # utiliser une valeur au carré pour que (-x) et (+ x) ne s'annulent pas
            total += j**2
        if total == 0:
            # append zero to list ONLY if all elements in a row are zero
            # ajouter zéro à la liste UNIQUEMENT si tous les éléments d'une ligne sont à zéro
            empty.append(total)
    # There is exactly one row with all zero elements if the following is true
    # Il y a exactement une ligne avec tous les éléments nuls si ce qui suit est vrai
    if len(empty) == 1:
        return True
    else:
        return False

# adds the objective function to the matrix.
# ajoute la fonction objectif à la matrice.


def obj(table, eq):
    if add_obj(table) == True:
        eq = [float(i) for i in eq.split(',')]
        lr = len(table[:, 0])
        row = table[lr-1, :]
        i = 0
    # iterate through all terms in the constraint function, excluding the last
    # parcourir tous les termes de la fonction de contrainte, à l'exclusion du dernier
        while i < len(eq)-1:
            # assign row values according to the equation
            # attribuer des valeurs de ligne en fonction de l'équation
            row[i] = eq[i]*-1
            i += 1
        row[-2] = 1
        row[-1] = eq[-1]
    else:
        print(
            'Vous devez terminer lajout des contraintes avant de pouvoir ajouter la fonction objectif.')

# solves maximization problem for optimal solution, returns dictionary w/ keys x1,x2...xn and max.
# résout le problème de maximisation pour une solution optimale, renvoie le dictionnaire avec les touches x1, x2 ... xn et max.


def maxz(table, output='summary'):
    while next_round_r(table) == True:
        table = pivot(loc_piv_r(table)[0], loc_piv_r(table)[1], table)
    while next_round(table) == True:
        table = pivot(loc_piv(table)[0], loc_piv(table)[1], table)

    lc = len(table[0, :])
    lr = len(table[:, 0])
    var = lc - lr - 1
    i = 0
    val = {}
    for i in range(var):
        col = table[:, i]
        s = sum(col)
        m = max(col)
        if float(s) == float(m):
            loc = np.where(col == m)[0][0]
            val[gen_var(table)[i]] = table[loc, -1]
        else:
            val[gen_var(table)[i]] = 0
    val['max'] = table[-1, -1]
    for k, v in val.items():
        val[k] = round(v, 6)
    if output == 'table':
        return table
    else:
        return val

# solves minimization problems for optimal solution, returns dictionary w/ keys x1,x2...xn and min.
# résout les problèmes de minimisation pour une solution optimale, renvoie le dictionnaire avec les clés x1, x2 ... xn et min.


def minz(table, output='summary'):
    table = convert_min(table)

    while next_round_r(table) == True:
        table = pivot(loc_piv_r(table)[0], loc_piv_r(table)[1], table)
    while next_round(table) == True:
        table = pivot(loc_piv(table)[0], loc_piv(table)[1], table)

    lc = len(table[0, :])
    lr = len(table[:, 0])
    var = lc - lr - 1
    i = 0
    val = {}
    for i in range(var):
        col = table[:, i]
        s = sum(col)
        m = max(col)
        if float(s) == float(m):
            loc = np.where(col == m)[0][0]
            val[gen_var(table)[i]] = table[loc, -1]
        else:
            val[gen_var(table)[i]] = 0
    val['min'] = table[-1, -1]*-1
    for k, v in val.items():
        val[k] = round(v, 6)
    if output == 'table':
        return table
    else:
        return val

# Prend en paramètres le type de probleme(m pour minimisation et M pour maximisation) et propose a lutilisateur un choix du probleme a resoudre


def resolvProblem(type):
    if(type == "m"):
        print("Vous avez choisi de résoudre un problème de minimisation\n")
    elif(type == "M"):
        print("Vous avez choisi de résoudre un problème de maximisation\n")
    constrains = []
    v = int(input("Entrez le nombre de variables de décision : "))
    c = int(input("Entrez le nombre de contraintes : "))
    print("Format d'une contrainte : 1,2, G, 10 signifiant 1 (x1) + 2 (x2) >= 10 \n")
    print("Entrez L au lieu de G pour le signe <= et E pour le signe = \n")
    print("Entrez vos contraintes : ")
    for i in range(c):
        constrain = input("Contrainte " + str(i+1) + "\n")
        constrains.append(constrain)
    print()
    print("Format d'une fonction Objectif : 1,2,0 signifiant 1 (x1) + 2 (x2) + 0 \n")
    object = input("Entrez la fonction Objectif  : \n")
    print("Solution : \n")
    solver(v, c, constrains, object, type)

# Prend en parametre le nombre de variables de décision, le nombre de contraintes, les contraintes, la fonction objectif et le type de probleme puis resous le probleme par appel de minz() ou maxz()


def solver(v, c, constrains, obje, type):
    m = gen_matrix(v, c)
    lenght = len(constrains)
    for i in range(lenght):
        constrain(m, constrains[i])
    obj(m, obje)
    if(type == "M"):
        print(maxz(m))
    elif(type == "m"):
        print(minz(m))


if __name__ == "__main__":
    rep = 1
    yn = 0

    while(rep):
        print("Quel type de problème lineaire souhaitez vous résoudre? : \n")
        choice = input(
            "============ 1) Problème de minimisation ============\n\n============ 2) Problème de maximisation ============\n\n============ 3) Quitter ============================= \n\n")

        if(int(choice) == 1):
            resolvProblem("m")
        elif(int(choice) == 2):
            resolvProblem("M")
        else:
            print("Bye...")
            rep = 0
